#define NOMINMAX
#include <cstdio>
#include <stdio.h>
#include <cstring>
#include <fstream>
#include <algorithm>    // std::max
#pragma warning(disable : 4996) // disable Compiler Warning (level 3) C4996 ,priek� microsoft compiler


int maksimalais_izmers = 15;
using namespace std;

typedef pair<int, int> ii;
//ii->pirmais parametrs ir index
//ii->otrais ir izmers

ii longestPrefix(char* content, int w, int size) {
	ii rezultats(0, 0);
	for (int i = max(0, w - 32), j = 0; i < w; i++) {
		for (j = 0; i + j < w && j + w < size && j < maksimalais_izmers; j++) {
			if (content[i + j] != content[w + j]) {
				break;
			}
		}
		if (j > rezultats.second) {
			rezultats = ii(w - i, j);
		}
	}
	return rezultats;
}

void dekompresesana(const char* ievaddati, const char* izvaddati) {
	FILE* fin = fopen(ievaddati, "rb");
	ofstream fout(izvaddati);
	string rezultats = "";
	unsigned char buffer[3]; //izmantojam simbolus kaa ciparus no 0 lidz 255
	while (fread(buffer, sizeof(buffer), 1, fin)) {
		int jump = buffer[0] << 4;
		int size = buffer[1];
		jump |= (size >> 4);
		size &= maksimalais_izmers;
		if (jump)
			rezultats += rezultats.substr(rezultats.size() - jump, size);
		char end = (char)buffer[2];
		rezultats += end;
	}
	fout << rezultats;
	fclose(fin);
}


void kompresesana(const char* ievaddati, const char* izvaddati) {
	FILE* fin = fopen(ievaddati, "rb");
	FILE* fout = fopen(izvaddati, "wb");
	fseek(fin, 0, SEEK_END);
	long size = ftell(fin);
	fseek(fin, 0, SEEK_SET);
	
	char* content = new char[size]; //jo tas ir variable length array un microsoft nelauj compileet ja tas nav dynamic array,bet gcc atlauj
	fread(content, size, 1, fin);
	for (int i = 0; i < size; i++) {
		ii longest = longestPrefix(content, i, size - 1);
		unsigned char buffer[3];
		memset(buffer, 0, sizeof(buffer));
		buffer[1] = longest.second & maksimalais_izmers;
		buffer[1] |= (longest.first & 15) << 4;
		buffer[0] = (longest.first >> 4) & 0xFF;
		i += longest.second;
		buffer[2] = content[i];
		fwrite(buffer, sizeof(buffer), 1, fout);
	}
	fclose(fin);
	fclose(fout);
}



int main(int argc, char* args[]) {
	if (argc < 4) {
		printf("piemers kompresijai: ./out k ievaddati izvaddati or\n");
		printf("piemers dekompresijai: ./a.out d ievaddati izvaddati\n");
		printf("opcijas: k->kompresesana, d->dekompresesana");
	}
	if (args[1][0] == 'k') {
		kompresesana(args[2], args[3]);
	}
	else {
		dekompresesana(args[2], args[3]);
	}
	return 0;
}